const express = require('express')
const mongoose = require('mongoose')
const csrf = require("csurf")
require('dotenv/config')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const cors = require('cors')
const admin = require("firebase-admin")

const csrfMiddleware = csrf({ cookie:true });

const serviceAccount = require("./findyo-staging-firebase-adminsdk-oar94-7da381ea01.json")

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://findyo-staging.firebaseio.com"
});

const app = express()

app.use(bodyParser.json())
app.use(cookieParser())
//app.use(csrfMiddleware)
app.use(cors())

//Take every request and set cookie XSRF-TOKEN
// app.all('*',(req,res,next)=>{
//   res.cookie("XSRF-TOKEN",req.csrfToken())
//   next()
// })

//Imports Routes
const usersRoutes = require('./routes/users')
app.use('/users', usersRoutes)



app.get('/',(req,res)=>{
  res.send("This is findyo API")
})

//Connect to DB
mongoose.connect(process.env.DB_CONNECTION,{ 
  useNewUrlParser: true,
  useUnifiedTopology: true
})
.then(()=>{
  console.log("App Server Successfully Connected with MongoDB Atlas");
})
.catch(error=>{
  console.log("Error Connecting App Server to MongoDB Atlas",error);
})

//Listning to server
app.listen(process.env.API_SERVER_PORT)


//Make these changes in the front end
//firebase.auth().setPersistance(firebase.auth.Auth.Persistance.NONE);
//Because we use our own persistant layer and session cookie 