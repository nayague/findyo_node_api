const express = require('express')
const app = express()
require('dotenv/config')
const jwt = require('jsonwebtoken')
const admin = require('firebase-admin')
const User = require('./models/User')
const logger = require('./logger')
const cors = require('cors')
const mongoose = require('mongoose')

app.use(express.json())
app.use(cors())


const serviceAccount = require("./findyo-staging-firebase-adminsdk-oar94-7da381ea01.json")

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://findyo-staging.firebaseio.com"
});

//Connect to DB
mongoose.connect(process.env.DB_CONNECTION,{ 
  useNewUrlParser: true,
  useUnifiedTopology: true
})
.then(()=>{
  console.log("Auth Server Successfully Connected with MongoDB Atlas");
})
.catch(error=>{
  console.log("Error Connecting Auth Server to MongoDB Atlas",error);
})

const posts = [

  {
    username: 'Kyle',
    title: 'Post 1'
  },
  {
    username: 'Jim',
    title: 'Post 2'
  }

]
const refreshTokens = []

const authenticateToken = (req,res,next) => {

  const authHeader = req.headers['authorization']
  const token = authHeader && authHeader.split(" ")[1]

  if(token == null) return res.sendStatus(401)

  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err,user)=>{
    if(err) return res.sendStatus(403)
    req.user = user
    next()
  })
}
const generateAccessToken = (userEmail) => {
  const expire =  process.env.TOKEN_EXPIRE_TIME
  const access_token = process.env.ACCESS_TOKEN_SECRET
  return jwt.sign({email:userEmail},access_token,{expiresIn: expire})
}
const validateToken = (firebaseID) => {
  return admin
    .auth()
    .verifyIdToken(firebaseID, true)
}
const getUserDetails = (user) => {

  return new Promise((resolve, reject) => {
    console.log("3.1");
    
    try {
      var query = User.findOne({ firebase_Id: user.uid })
      
      query.select('email')

      console.log("3.2");

      query.exec(function (err, localUser) {
        console.log("3.3");
        if (err) {
          console.log("3.4");
          reject(err)
        } else {
          console.log("3.5");
          resolve({ localUser, user })
        }
      });
    } catch (ex) {
      console.log("3.6");
      reject(ex)
    }
  })
}

app.get('/posts',authenticateToken,(req,res)=>{
  res.json(posts.filter(post => post.username === req.user.name));
})
app.post('/login',(req,res)=>{
  
  try{
    
    if(req.body.TokenID == null)
      return res.sendStatus(401)

      
    const idToken = req.body.TokenID.toString()
    console.log(idToken);

    validateToken(idToken)
    .then(result=>{
      return getUserDetails(result)
    })
    .then(result=>{
      if (result.localUser !== null) {
        //Create JWT
        const email = result.localUser.email
        const accessToken = generateAccessToken(email)
        const refreshToken = jwt.sign(email,process.env.REFRESH_TOKEN_SECRET)
        res.json({accessToken: accessToken, refreshToken : refreshToken})
      } else {
        //Create new user
        //addNewUser(result.user)
        res.send("New User")
        //CreateJWT
      }
    })
    .catch(err=>{
      console.log(err);
      
      logger.log('error',err)
      res.sendStatus(401)
    })
  }catch(ex){
    console.log(ex)
    logger.log('error',ex)
    res.sendStatus(401)
  }
  
})
app.post('/token',(req,res)=>{
  const refreshToken = req.body.token
  if(refreshToken == null) return res.sendStatus(401)
  if(!refreshTokens.includes(refreshToken)) res.sendStatus(401)

  jwt.verify(refreshToken,process.env.REFRESH_TOKEN_SECRET,(err,user)=>{
    if(err) return res.sendStatus(403)
    const accessToken = generateAccessToken({name:user.name})
    res.json({accessToken: accessToken})
  })
})
app.get('/log',(req,res)=>{
  let err = "this is my error"
  logger.log('error',err)
})

app.listen(process.env.AUTH_SERVER_PORT)