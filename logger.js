const {createLogger, transports, format} = require('winston')

const logger = createLogger({
  transports:[
    new transports.Console({
      format: format.combine(format.timestamp(),format.simple())
    }),
    new transports.File({
      filename:'error.log',
      format: format.combine(format.timestamp(),format.simple())
    })
  ]
})

module.exports = logger