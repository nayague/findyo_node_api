const mongoose = require('mongoose')

const UserSchema = mongoose.Schema({
  username:  String,
  displayName: String,
  first_name: String,
  last_name: String,
  email: String,
  about_you: String,
  address: String,
  city: String,
  district: String,
  province: String,
  country: String,
  postal_code: String,
  coverURL: String,
  creationTime: {
    type: String,
    default: Date.now()
  },
  date_of_birth: Date,
  job_title: String,
  phoneNumber: [String],
  photoURL: String,
  preferences_and_interests: [String],
  skills: [String],
  firebase_Id: {
    type: String,
    required: true
  }

})

module.exports = mongoose.model('Users', UserSchema)