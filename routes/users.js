const express = require('express')
const router = express.Router()
const User = require('../models/User')
const adimn = require('firebase-admin')

router.get('/', /*async*/(req, res) => {
  try {
    //const posts = await Post.find()
    res.json("API Hitted")
  } catch (e) {
    res.json(e)
  }
})

//Add new user after signing up with firebase auth
router.post('/', async (req, res) => {
  try {

    let body = req.body

    if ((body.firebase_Id == "") || (body.email == "" && body.phoneNumber == "")) {
      res.status(400).json({ message: "Email or Password must be filled" })
    }

    const user = new User({
      email: body.email,
      phoneNumber: body.phoneNumber,
      creationTime: body.creationTime,
      firebase_Id: body.firebase_Id
    })

    const savedPost = await user.save()

    res.json(savedPost)

  } catch (er) {
    res.status(500)
  }
})

router.post('/sessionLogin', (req, res) => {

  const idToken = req.body.TokenID.toString();
  //const expiresIn = 60 * 60 * 24 * 5 * 1000;
  // console.log("Hit");


  if (idToken.length > 0) {
    // console.log("Has Token");
    validateToken(idToken)
      .then(result => {
        // console.log("Has Result");
        // console.log(result);
        return getUserDetails(result)
        //res.status(200).end()
      })
      .then(result => {
        //console.log(result);

        if (result.localUser !== null) {
          //Create JWT
          res.send("Has User")
        } else {
          //Create new user
          addNewUser(result.user)
          res.send("New User")
          //CreateJWT
        }
      })
      .catch(err => {
        console.log(err)
        res.status(401)
      })

  }

})

router.get('/profile', (req, res) => {

  let sessionCookie = req.cookies.session || "";

  console.log("SessionID : ", sessionCookie);


  adimn.auth()
    .verifyIdToken(sessionCookie, true)
    .then(() => {
      console.log("Authenticated")
      res.end()
    })
    .catch(err => {
      console.log(err)
      res.end()
    })

})

const getUserDetails = (user) => {

  return new Promise((res, rej) => {

    try {
      var query = User.findOne({ firebase_Id: user.uid })
      query.select('username displayName email job_title photoURL')

      query.exec(function (err, localUser) {

        if (err) {
          rej(err)
        } else {
          res({ localUser, user })
        }
      });
    } catch (ex) {
      rej(ex)
    }

  })


}
const validateToken = (idToken) => {

  return adimn
    .auth()
    .verifyIdToken(idToken, true)

}
const addNewUser = (firebaseUser) => {

  return new Promise((res, rej) => {

    try {

      const user = new User({
        email: firebaseUser.email,
        firebase_Id: firebaseUser.uid,
        creationTime: firebaseUser.auth_time,
        photoURL: firebaseUser.picture
      })

      res(user.save())

    } catch (ex) {
      rej(ex)
    }
  })

}

module.exports = router